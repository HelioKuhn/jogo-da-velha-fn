import { useEffect, useState } from 'react'
import './Game.css'

function Game() {

  
  const iniciarTabuleiroVazio = Array(9).fill("");

  const [tabuleiro, setTabuleiro] = useState(iniciarTabuleiroVazio);

  const [vencedor, setVencedor] = useState();

  const [jogador, setJogador] = useState('X')

  

  const setaValorTabuleiro = (index) => {
    if(tabuleiro[index]!='') return null
    setTabuleiro(tabuleiro.map((item, itemIndex) => itemIndex === index ? jogador : item));
    setJogador((jogador) => jogador === 'X'? 'O': 'X');
  }

  const checarVencedor = () =>{
    const possibilades = [
      [tabuleiro[0],tabuleiro[1],tabuleiro[2]],
      [tabuleiro[3],tabuleiro[4],tabuleiro[5]],
      [tabuleiro[6],tabuleiro[7],tabuleiro[8]],

      [tabuleiro[0],tabuleiro[3],tabuleiro[6]],
      [tabuleiro[1],tabuleiro[4],tabuleiro[7]],
      [tabuleiro[2],tabuleiro[5],tabuleiro[8]],

      [tabuleiro[0],tabuleiro[4],tabuleiro[8]],
      [tabuleiro[2],tabuleiro[4],tabuleiro[6]],
    ]

    possibilades.forEach(cells=>{
     if (cells.every(cell=> cell==='X')) setVencedor("X")
     if (cells.every(cell=> cell==='O')) setVencedor("O")
    })
  }

  useEffect(checarVencedor,[tabuleiro]);


  const reiniciarPartida = () =>{
    setTabuleiro(iniciarTabuleiroVazio);
    setVencedor(null)
  }

  return (
    <main>
      <div className='titulo'>Jogo da velha</div>

      <div className={`tabuleiro ${vencedor ? "fim-de-jogo": ""}`}>
        {tabuleiro.map((valor, index) => (
          <div 
            key={index}
            className={`celula ${valor}`}
            onClick = {() => setaValorTabuleiro(index)}
            > 
          </div>
        ))}
      </div>
          {vencedor &&
          <footer>
            <h2 className='h2-vencedor'><span className={`vencedor-${vencedor}`}>{vencedor}</span> Venceu!!!
              <button className='botao' onClick={reiniciarPartida}>Reiniciar partida</button>
            </h2>
          </footer>
      }
    </main>
  )
}



export default Game
